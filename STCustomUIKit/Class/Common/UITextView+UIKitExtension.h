//
//  UITextView+UIKitExtension.h
//  STCustomUIKit
//
//  Created by jerry on 2019/5/1.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UITextView (UIKitExtension)

/** placeholdLabel */
@property(nonatomic,readonly)  UILabel *placeholdLabel;
/** placeholder */
@property(nonatomic,copy) NSString *placeholder;
/** placeholder颜色 */
@property(nonatomic,copy) UIColor *placeholderColor;
/** 富文本 */
@property(nonnull,strong) NSAttributedString *attributePlaceholder;
/** 位置 */
@property(nonatomic,assign) CGPoint location;
/** 默认颜色 */
+ (UIColor *)defaultColor;

@end

NS_ASSUME_NONNULL_END
