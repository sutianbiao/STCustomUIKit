//
//  STDoubleSpaceSlider.h
//  STCustomUIKit
//  区间滑动选择(常用于选择一定的范围)
//  Created by jerry on 2019/5/1.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface STDoubleSpaceSlider : UIView

@property (nonatomic, assign) CGFloat curMinValue;//当前最小的值
@property (nonatomic, assign) CGFloat curMaxValue;//当前最大的值
@property (nonatomic, assign) BOOL needAnimation;//是否需要动画
@property (nonatomic, assign) CGFloat minInterval;//间隔大小

@property (nonatomic, strong) UIColor *minTintColor;
@property (nonatomic, strong) UIColor *midTintColor;
@property (nonatomic, strong) UIColor *maxTintColor;

@property (nonatomic, copy)  void (^sliderBtnLocationChangeBlock)(BOOL isLeft, BOOL finish);//滑块位置改变后的回调 isLeft 是否是左边 finish手势是否结束

@end

NS_ASSUME_NONNULL_END
