//
//  STScrollRulerView.h
//  STCustomUIKit
//  滑动选择游标尺子（常用于体重、身高等滑动）
//  Created by jerry on 2019/5/1.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class STScrollRulerView;
@protocol STScrollRulerViewDelegate <NSObject>
/*
 *  游标卡尺滑动，对应value回调
 *  滑动视图
 *  当前滑动的值
 */
-(void)dyScrollRulerView:(STScrollRulerView *)rulerView valueChange:(float)value;

@end

@interface STScrollRulerView : UIView

@property(nonatomic,weak)id<STScrollRulerViewDelegate>    delegate;

//滑动时是否改变textfield值
@property(nonatomic, assign)BOOL scrollByHand;

//三角形颜色
@property(nonatomic,strong)UIColor *triangleColor;
//背景颜色
@property(nonatomic,strong)UIColor *bgColor;

-(instancetype)initWithFrame:(CGRect)frame theMinValue:(float)minValue theMaxValue:(float)maxValue theStep:(float)step theUnit:(NSString *)unit theNum:(NSInteger)betweenNum;

-(void)setRealValue:(float)realValue animated:(BOOL)animated;

+(CGFloat)rulerViewHeight;

-(void)setDefaultValue:(float)defaultValue animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
