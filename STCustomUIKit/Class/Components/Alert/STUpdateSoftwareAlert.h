//
//  STUpdateSoftwareAlert.h
//  STCustomUIKit
//  软件版本更新Alert
//  Created by jerry on 2019/5/1.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface STUpdateSoftwareAlert : UIView

@property (nonatomic,copy) void (^clickGoAppStoreBtnBlock)(void);
@property (nonatomic,copy) void (^clickCloseBtnBlock)(void);

@property (nonatomic,assign) int mustUpdateFlag; //1为强制更新

-(instancetype)initWithTitle:(NSString *)title andShowTxt:(NSString *)showTxt;

#pragma mark -method

-(void)show;
-(void)dissmiss;

@end

NS_ASSUME_NONNULL_END
