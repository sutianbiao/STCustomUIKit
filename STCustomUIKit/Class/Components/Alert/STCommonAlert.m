//
//  STCommonAlert.m
//  STCustomUIKit
//
//  Created by jerry on 2019/5/1.
//  Copyright © 2019 jerry. All rights reserved.
//

#import "STCommonAlert.h"
#import <Masonry.h>

#define S_TOP_TITLE_HEIGHT  40.0f
#define S_BOTTOM_BTN_HEIGHT 40.0f

#define S_SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define S_SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height

#define S_CANCLE_BTN_COLOR  [UIColor colorWithRed:204/255.0f green:204/255.0f blue:204/255.0f alpha:1]
#define S_CONFIRM_BTN_COLOR [UIColor colorWithRed:238/255.0f green:79/255.0f blue:82/255.0f alpha:1]
#define S_TITLE_COLOR       [UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1]
#define S_CONTENT_COLOR     [UIColor colorWithRed:176/255.0f green:176/255.0f blue:176/255.0f alpha:1]
#define S_LINE_COLOR        [UIColor colorWithRed:242/255.0f green:242/255.0f blue:242/255.0f alpha:1]

@interface STCommonAlert()

@property (nonatomic, strong) UIView      * contentView;
@property (nonatomic, strong) UIImageView * contentIv;

@property (nonatomic, strong) UILabel  *titleLabel;
@property (nonatomic, strong) UILabel  *contentLabel;

@property (nonatomic, strong) UIView  *line0;
@property (nonatomic, strong) UIView  *line1;
@property (nonatomic, strong) UIView  *line2;

@property (nonatomic, strong) UIButton *btnCancel;
@property (nonatomic, strong) UIButton *btnSure;

@end


@implementation STCommonAlert

-(instancetype)initWithContentMessage:(NSString *)messageStr confirmTitle:(NSString *)confirmTitleStr{
    self = [super init];
    if (self) {
       [self showWithTitle:nil Message:messageStr cancel:nil confirm:confirmTitleStr];
    }
    
    return self;
}

-(instancetype)initWithTitle:(NSString *)titleStr contentMessage:(NSString *)messageStr confirmTitle:(NSString *)confirmTitleStr{
    self = [super init];
    if (self) {
       [self showWithTitle:titleStr Message:messageStr cancel:nil confirm:confirmTitleStr];
    }
    
    return self;
}

-(instancetype)initWithTitle:(NSString *)titleStr contentMessage:(NSString *)messageStr cancelTitle:(NSString *)cancelTitleStr confirmTitle:(NSString *)confirmTitleStr{
    
    self = [super init];
    if (self) {
        [self showWithTitle:titleStr Message:messageStr cancel:cancelTitleStr confirm:confirmTitleStr];
    }
    
    return self;
}

- (void)showWithTitle:(NSString *)title Message:(NSString *)message cancel:(NSString *)cancel confirm:(NSString *)confirm {
    self.frame = [UIScreen mainScreen].bounds;
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    //初始化
    [self initRefresVars];
    [self initRefresViews];
    
    CGFloat w = 0;
    CGFloat h = 0;
    if (self.alertWidth>0 && self.alertHeight>0) {
        //优先固定大小
        w = self.alertWidth;
        h = self.alertHeight;
    }else{
        //动态比例大小
        w = S_SCREEN_WIDTH * self.percentWidth;
        h = w * 0.6;
    }
    
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(w, h));
    }];
    
    [self.contentIv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
    }];
    
    
    [self.line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-S_BOTTOM_BTN_HEIGHT-0.5);
        make.height.equalTo(@0.5);
        make.left.right.equalTo(self.contentView);
    }];
    
    if ((confirm && ![confirm isEqualToString:@""]) && (cancel && ![cancel isEqualToString:@""])) {
        //两个按钮
        [self.line2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.line1.mas_bottom);
            make.centerX.equalTo(self.contentView.mas_centerX);
            make.bottom.equalTo(self.contentView.mas_bottom);
            make.width.equalTo(@0.5);
        }];
        
        [self.btnCancel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left);
            make.right.equalTo(self.line2.mas_left);
            make.top.equalTo(self.line1.mas_bottom);
            make.bottom.equalTo(self.contentView.mas_bottom);
            make.width.equalTo(self.btnSure.mas_width);
        }];
        [self.btnCancel setTitle:cancel forState:UIControlStateNormal];
        
        [self.btnSure mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_right);
            make.left.equalTo(self.line2.mas_right);
            make.top.equalTo(self.line1.mas_bottom);
            make.bottom.equalTo(self.contentView.mas_bottom);
            make.width.equalTo(self.btnCancel.mas_width);
        }];
        [self.btnSure setTitle:confirm forState:UIControlStateNormal];
        
    }else{
        //一个按钮
        [self.btnSure mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.line1.mas_bottom);
            make.left.right.bottom.equalTo(self.contentView);
        }];
        [self.btnSure setTitle:confirm forState:UIControlStateNormal];
    }
    
    //判断是否有标题
    if (title && ![title isEqualToString:@""]) {
        //有标题
        [self.line0 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView.mas_top).offset(S_TOP_TITLE_HEIGHT-0.5);
            make.height.equalTo(@0.5);
            make.left.right.equalTo(self.contentView);
        }];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.contentView.mas_centerX);
            make.left.equalTo(self.contentView.mas_left).offset(12);
            make.right.equalTo(self.contentView.mas_right).offset(-12);
            make.height.equalTo(@S_TOP_TITLE_HEIGHT);
        }];
        self.titleLabel.text = title;
        
        [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).offset(12);
            make.right.equalTo(self.contentView.mas_right).offset(-12);
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.height.lessThanOrEqualTo(@(h-S_TOP_TITLE_HEIGHT-S_BOTTOM_BTN_HEIGHT));
        }];
        self.contentLabel.text = message;
        
    }else{
        //没有标题
        [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView.mas_left).offset(12);
            make.right.equalTo(self.contentView.mas_right).offset(-12);
            make.centerY.equalTo(self.contentView.mas_centerY);
            make.height.lessThanOrEqualTo(@(h-S_BOTTOM_BTN_HEIGHT));
        }];
        self.contentLabel.text = message;
        
    }
    
    [self.contentView bringSubviewToFront:self.line0];
    [self.contentView bringSubviewToFront:self.line1];
    [self.contentView bringSubviewToFront:self.line2];
    [self.contentView bringSubviewToFront:self.btnCancel];
    [self.contentView bringSubviewToFront:self.btnSure];
    [self.contentView bringSubviewToFront:self.titleLabel];
    [self.contentView bringSubviewToFront:self.contentLabel];
    
}

- (void)initRefresVars{
    _alertWidth = self.alertWidth<=0 ? 0 :self.alertWidth;
    _alertHeight = self.alertHeight<=0 ? 0 :self.alertHeight;
    _percentWidth = (self.percentWidth<=0 || self.percentWidth>=1) ? 0.8 :self.percentWidth;
    _bgColor = (self.bgColor && [self.bgColor isKindOfClass:[UIColor class]]) ? self.bgColor :[UIColor whiteColor];
    _bgImage = self.bgImage ? self.bgImage :nil;
    _cancleColor = (self.cancleColor && [self.cancleColor isKindOfClass:[UIColor class]]) ? self.cancleColor: S_CANCLE_BTN_COLOR;
    _confirmColor = (self.confirmColor && [self.confirmColor isKindOfClass:[UIColor class]]) ? self.confirmColor: S_CONFIRM_BTN_COLOR;
    _titleColor = (self.titleColor && [self.titleColor isKindOfClass:[UIColor class]]) ? self.titleColor :S_TITLE_COLOR;
    _contentColor = (self.contentColor && [self.contentColor isKindOfClass:[UIColor class]]) ? self.contentColor :S_CONTENT_COLOR;
    _btnTxtColor = (self.btnTxtColor && [self.btnTxtColor isKindOfClass:[UIColor class]]) ? self.btnTxtColor :[UIColor whiteColor];
    _lineColor = (self.lineColor && [self.lineColor isKindOfClass:[UIColor class]]) ? self.lineColor :S_LINE_COLOR;
    _titleFont = (self.titleFont>0 && self.titleFont<30) ? self.titleFont : 18.0f;
    _contentFont = (self.contentFont>0 && self.contentFont<30) ? self.contentFont : 16.0f;
    _btnFont = (self.btnFont>0 && self.btnFont<30) ? self.btnFont : 16.0f;
    _cornerRadius = (self.cornerRadius>0 && self.cornerRadius<30) ? self.cornerRadius : 0;
}

- (void)initRefresViews{
    //设置圆角以及背景颜色
    self.contentView.backgroundColor = self.bgColor;
    self.contentView.layer.cornerRadius = self.cornerRadius;
    self.contentView.layer.masksToBounds= YES;
    
    //设置背景图片
    if (self.bgImage && ![self.bgImage isEqualToString:@""]) {
        [self.contentIv setImage:[UIImage imageNamed:self.bgImage]];
    }
    
    //设置文字颜色大小
    self.titleLabel.font = [UIFont systemFontOfSize:self.titleFont];
    self.contentLabel.font = [UIFont systemFontOfSize:self.contentFont];
    self.titleLabel.textColor = self.titleColor;
    self.contentLabel.textColor = self.contentColor;
    
    //设置间隔线颜色
    self.line0.backgroundColor = self.lineColor;
    self.line1.backgroundColor = self.lineColor;
    self.line2.backgroundColor = self.lineColor;
    
    //按钮颜色字体
    [self.btnCancel setTitleColor:self.btnTxtColor forState:UIControlStateNormal];
    [self.btnSure setTitleColor:self.btnTxtColor forState:UIControlStateNormal];
    [self.btnCancel setBackgroundColor:self.cancleColor];
    [self.btnSure setBackgroundColor:self.confirmColor];
    
    self.btnCancel.titleLabel.font = [UIFont systemFontOfSize:self.btnFont];
    self.btnSure.titleLabel.font = [UIFont systemFontOfSize:self.btnFont];
}

- (void)initRefresUpdate{
    
    CGFloat w = 0;
    CGFloat h = 0;
    if (self.alertWidth>0 && self.alertHeight>0) {
        //优先固定大小
        w = self.alertWidth;
        h = self.alertHeight;
    }else{
        //动态比例大小
        w = S_SCREEN_WIDTH * self.percentWidth;
        h = w * 0.6;
    }
    
    [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mas_centerX);
        make.centerY.equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(w, h));
    }];
    
    [self.contentIv mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView.mas_top);
        make.bottom.equalTo(self.contentView.mas_bottom);
        make.left.equalTo(self.contentView.mas_left);
        make.right.equalTo(self.contentView.mas_right);
    }];
    
}

- (void)initRefresUpdateStyle{
    if (_alertType == STCommonAlertTypeNormal) {
        
        
    }else if(_alertType == STCommonAlertTypeRound){
        
        
    }
}

#pragma mark -method

-(void)show{
    // 出场动画
    self.alpha = 0;
    self.contentView.transform = CGAffineTransformScale(self.contentView.transform, 1.3, 1.3);
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 1;
        self.contentView.transform = CGAffineTransformIdentity;
    }];
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    [window addSubview:self];
}

-(void)dissmiss{
   [self removeFromSuperview];
}

#pragma mark -set/get

- (UIView *)contentView{
    if (!_contentView) {
        _contentView = [[UIView alloc]init];
        [self addSubview:_contentView];
    }
    return _contentView;
}

- (UIImageView *)contentIv{
    if (!_contentIv) {
        _contentIv = [[UIImageView alloc]init];
        [self.contentView addSubview:_contentIv];
    }
    return _contentIv;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}

- (UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc]init];
        _contentLabel.numberOfLines = 0;
        [self.contentView addSubview:_contentLabel];
    }
    return _contentLabel;
}

- (UIView *)line0{
    if (!_line0) {
        _line0 = [[UIView alloc]init];
        [self.contentView addSubview:_line0];
    }
    return _line0;
}

- (UIView *)line1{
    if (!_line1) {
        _line1 = [[UIView alloc]init];
        [self.contentView addSubview:_line1];
    }
    return _line1;
}

- (UIView *)line2{
    if (!_line2) {
        _line2 = [[UIView alloc]init];
        [self.contentView addSubview:_line2];
    }
    return _line2;
}

- (UIButton *)btnCancel{
    if (!_btnCancel) {
        _btnCancel = [[UIButton alloc]init];
        [_btnCancel addTarget:self action:@selector(cancleAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_btnCancel];
    }
    return _btnCancel;
}

- (UIButton *)btnSure{
    if (!_btnSure) {
        _btnSure = [[UIButton alloc]init];
        [_btnSure addTarget:self action:@selector(sureAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_btnSure];
    }
    return _btnSure;
}

#pragma mark - Action

///取消
-(void)cancleAction:(UIButton *)sender{
    if (self.clickCancelBtnBlock) {
        self.clickCancelBtnBlock();
    }
    [self dissmiss];
}

///确定
-(void)sureAction:(UIButton *)sender{
    if (self.clickOkBtnBlock) {
        self.clickOkBtnBlock();
    }
    [self dissmiss];
}

#pragma mark - Methods

- (void)setAlertType:(STCommonAlertType)alertType{
    _alertType = alertType;
    [self initRefresUpdateStyle];
}

- (void)setAlertWidth:(CGFloat)alertWidth{
    _alertWidth = alertWidth;
    [self initRefresVars];
    [self initRefresViews];
    [self initRefresUpdate];
}

- (void)setAlertHeight:(CGFloat)alertHeight{
    _alertHeight = alertHeight;
    [self initRefresVars];
    [self initRefresViews];
    [self initRefresUpdate];
}

- (void)setPercentWidth:(CGFloat)percentWidth{
    _percentWidth = percentWidth;
    [self initRefresVars];
    [self initRefresViews];
    [self initRefresUpdate];
}

- (void)setBgColor:(UIColor *)bgColor{
    _bgColor = bgColor;
    [self initRefresVars];
    [self initRefresViews];
}

- (void)setBgImage:(NSString *)bgImage{
    _bgImage = bgImage;
    [self initRefresVars];
    [self initRefresViews];
}

- (void)setCancleColor:(UIColor *)cancleColor{
    _cancleColor = cancleColor;
    [self initRefresVars];
    [self initRefresViews];
}

- (void)setConfirmColor:(UIColor *)confirmColor{
    _confirmColor = confirmColor;
    [self initRefresVars];
    [self initRefresViews];
}

- (void)setBtnTxtColor:(UIColor *)btnTxtColor{
    _btnTxtColor = btnTxtColor;
    [self initRefresVars];
    [self initRefresViews];
}

- (void)setTitleColor:(UIColor *)titleColor{
    _titleColor = titleColor;
    [self initRefresVars];
    [self initRefresViews];
}

- (void)setContentColor:(UIColor *)contentColor{
    _contentColor = contentColor;
    [self initRefresVars];
    [self initRefresViews];
}

- (void)setLineColor:(UIColor *)lineColor{
    _lineColor = lineColor;
    [self initRefresVars];
    [self initRefresViews];
}

- (void)setTitleFont:(CGFloat)titleFont{
    _titleFont = titleFont;
    [self initRefresVars];
    [self initRefresViews];
}

- (void)setContentFont:(CGFloat)contentFont{
    _contentFont = contentFont;
    [self initRefresVars];
    [self initRefresViews];
}

- (void)setBtnFont:(CGFloat)btnFont{
    _btnFont = btnFont;
    [self initRefresVars];
    [self initRefresViews];
}

- (void)setCornerRadius:(CGFloat)cornerRadius{
    _cornerRadius = cornerRadius;
    [self initRefresVars];
    [self initRefresViews];
}

@end
