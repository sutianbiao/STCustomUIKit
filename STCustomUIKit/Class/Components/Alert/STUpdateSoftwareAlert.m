//
//  STUpdateSoftwareAlert.m
//  STCustomUIKit
//
//  Created by jerry on 2019/5/1.
//  Copyright © 2019 jerry. All rights reserved.
//

#import "STUpdateSoftwareAlert.h"
#import <Masonry.h>

@interface STUpdateSoftwareAlert()

@property (nonatomic,copy)NSString   * title;
@property (nonatomic,copy)NSString   * showTxt;

@property (nonatomic,strong)UILabel  * titleLabel;
@property (nonatomic,strong)UILabel  * contentLabel;

@property (nonatomic,strong)UIButton * closeBtn;
@property (nonatomic,strong)UIButton * confirmBtn;

@property (nonatomic,strong)UIImageView * bgIv;

@end

@implementation STUpdateSoftwareAlert

-(instancetype)initWithTitle:(NSString *)title andShowTxt:(NSString *)showTxt{
    
    self = [super init];
    if (self) {
        
        if(title && ![title isEqualToString:@""]){
            self.title = title;
        }
        
        if(showTxt && ![showTxt isEqualToString:@""]){
            self.showTxt = showTxt;
        }
        
        [self showView];
        
    }
    return self;
}

-(void)showView{
    
    
}

#pragma mark -method

-(void)show{
    
}

-(void)dissmiss{
    
}

#pragma mark -set/get



@end
