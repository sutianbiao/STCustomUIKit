//
//  STInputAlert.h
//  STCustomUIKit
//  单输入框Alert
//  Created by jerry on 2019/5/1.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    STInputAlertShowTypeAlert,
    STInputAlertShowTypeSheet
} STInputAlertShowType;

@interface STInputAlert : UIView

@end

NS_ASSUME_NONNULL_END
