//
//  STSingleChooseListAlert.h
//  STCustomUIKit
//  单选列表Alert
//  Created by jerry on 2019/5/1.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum : NSUInteger {
    STSingleChooseListAlertShowTypeAlert,
    STSingleChooseListAlertShowTypeSheet
} STSingleChooseListAlertShowType;

@interface STSingleChooseListAlert : UIView





@end

NS_ASSUME_NONNULL_END
