//
//  STCommonAlert.h
//  STCustomUIKit
//  通用Alert
//  Created by jerry on 2019/5/1.
//  Copyright © 2019 jerry. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum {
    STCommonAlertTypeNormal = 0,  // 默认
    STCommonAlertTypeRound  = 1,  //
}STCommonAlertType;

@interface STCommonAlert : UIView

@property (nonatomic,assign) STCommonAlertType alertType; //弹窗样式

@property (nonatomic,assign) CGFloat alertWidth;  //固定大小的宽度(非必须)
@property (nonatomic,assign) CGFloat alertHeight; //固定大小的高度(非必须)

@property (nonatomic,assign) CGFloat percentWidth;//弹窗相对于屏幕百分比

@property (nonatomic,strong) UIColor * bgColor;   //弹窗背景颜色
@property (nonatomic,copy)   NSString* bgImage;   //弹窗背景图片

@property (nonatomic,strong) UIColor * cancleColor;    //弹窗取消按钮颜色
@property (nonatomic,strong) UIColor * confirmColor;   //弹窗确定按钮颜色
@property (nonatomic,assign) UIColor * btnTxtColor;    //按钮文字颜色

@property (nonatomic,strong) UIColor * titleColor;     //标题文字颜色
@property (nonatomic,strong) UIColor * contentColor;   //内容文字颜色

@property (nonatomic,strong) UIColor * lineColor;      //弹窗间隔线段颜色

@property (nonatomic,assign) CGFloat titleFont;        //标题字体大小
@property (nonatomic,assign) CGFloat contentFont;      //内容字体大小
@property (nonatomic,assign) CGFloat btnFont;          //按钮字体大小

@property (nonatomic,assign) CGFloat cornerRadius;     //弹窗圆角大小

@property (nonatomic,copy) void (^clickOkBtnBlock)(void);
@property (nonatomic,copy) void (^clickCancelBtnBlock)(void);

-(instancetype)initWithContentMessage:(NSString *)messageStr confirmTitle:(NSString *)confirmTitleStr;

-(instancetype)initWithTitle:(NSString *)titleStr contentMessage:(NSString *)messageStr confirmTitle:(NSString *)confirmTitleStr;

-(instancetype)initWithTitle:(NSString *)titleStr contentMessage:(NSString *)messageStr cancelTitle:(NSString *)cancelTitleStr confirmTitle:(NSString *)confirmTitleStr;

#pragma mark -method

-(void)show;
-(void)dissmiss;

@end

NS_ASSUME_NONNULL_END
